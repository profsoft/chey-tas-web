import React from "react";
import { Route, NavLink, Switch, Link } from "react-router-dom";
import yellowStar from "./images/yellowStar.png";
import star from "./images/star.png";
import icon from "./images/favicon.png";
import "./style.css";
import * as R from "ramda";
import { CSSTransition, TransitionGroup } from "react-transition-group";

const Nav = () => {
    return (
        <header>
            <div className="navigation">
                <div className="navigation-top">
                    <img 
                        src={ icon }
                        alt="icon"
                    />
                    <NavLink exact to="/">Weather Forecast</NavLink>
                </div>
            </div>
            <hr />

        </header>
    )
};


class Main extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cities: [],
            weather: [],
            favourites: {},
            location: "",
            ulr: ""
        }
    }

    addToFavourites(id) {
        let { cities, favourites } = this.state;

        this.setState({
            favourites: R.assoc(cities[id].title, cities[id].woeid, favourites)
        })
    }

    removeFromFavourites(id) {
        let { favourites } = this.state;
        let favouritesId = R.keys(favourites);

        this.setState({
            favourites: R.dissoc( favouritesId[id], favourites )
        })
    }

    componentWillMount() {
        localStorage.getItem('city') && this.setState({
            favourites: JSON.parse(localStorage.getItem('city'))
        })
    }
    
    getWeather(woeid) {
        fetch(`http://localhost:8089/city/${ woeid }`)
            .then((res) => res.json())
            .then((res) => {
                this.setState({
                    weather: res.data.consolidated_weather,
                    location: res.data.title + " " + res.data.parent.title
                })
            })
            .catch((err) => alert("Ошибка :" + err))
    }

    searchCity(city) {
        if (city.trim() === '') {
            this.setState({ cities: [] })
            return;
        }
        this.setState({ url: city })
        this.showCity(city);
    }

    showCity(url) {
        fetch(`http://localhost:8089/weather/${url}`)
            .then(res => res.json())
            .then(json => this.setState({ cities: json.data }));
    }

    // this allows dynamically show list of citis
    renderCity() {
        let { favourites, cities } = this.state;
        let favouritesTitles = R.keys(favourites);
        const verify = (xs, x) => R.indexOf(x, xs) === -1 ? true : false;

        return cities.map((_, i) => {
            return (
                <li className="cities-list-item" key={i}>
                    <span 
                        className="cities-list-item-span"
                        onClick={()=>this.getWeather( cities[i].woeid) }>
                        <Link 
                            to={`/city/${i}`}>{cities[i].title}
                        </Link>
                    </span>
                    <button 
                        onClick={ () => this.addToFavourites(i) } 
                        className="btn-star">
                    <img 
                        className="btn-star-img"
                        src={verify(favouritesTitles, cities[i].title)?  star : yellowStar} 
                        alt="star"
                    />
                    </button>
                </li>
            )
        })
    }

    render() {
        let { location, favourites } = this.state;
        let weather = this.state.weather.slice(0, 5);

        return (
            <div>
            <Nav />
                <Switch>
                    <TransitionGroup>
                    <Route exact path="/" render={ ()=> 
                        <CSSTransition 
                        in={true}
                        appear={true}
                        timeout={300}
                        classNames='search-animation'
                        >
                        <div className="search">
                           
                            <div>
                                <input 
                                    className="search-input"
                                    type="text"
                                    placeholder="Search for a city"
                                    onChange={(e) => this.searchCity( e.target.value )}
                                />
                            </div> 
                            <div className = "cities">
                                <ul className="cities-list">
                                    { this.renderCity() }
                                </ul>

                            </div> 
                            
                        </div>
                        </CSSTransition>
                    }/>
                    </TransitionGroup>
                </Switch> 
            </div>
        )
    }
}

export { Main };